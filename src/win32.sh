#!/usr/bin/env sh



##----------------------------------------------------------------------------##
## Public API Implementation                                                  ##
##----------------------------------------------------------------------------##
_pw_impl_get_user_home()
{
    local TARGET_USER="$1";
    test -z "$TARGET_USER" && TARGET_USER=$(whoami); ## No user given, default to current one.

    ## @XXX(stdmatt): How reliable get the home directory for the user in windows???
    local HOMEDIR=$(echo "$HOME");
    echo "$HOMEDIR";
}

##------------------------------------------------------------------------------
_pw_impl_os_get_simple_name()
{
    echo "$(PW_OS_WINDOWS)";
}

###-----------------------------------------------------------------------------
_pw_imp_get_file_mode()
{
    stat -c "%a" "$1";
}

###-----------------------------------------------------------------------------
_pw_imp_get_file_owner()
{
    stat -c "%U" "$1";
}

###-----------------------------------------------------------------------------
_pw_imp_get_file_group()
{
    stat -c "%G" "$1";
}